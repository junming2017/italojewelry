<?php
declare(strict_types=1);
/**
 * PHP version 5.5
 *
 * @copyright  Copyright (c) 2012-2015 EELLY Inc. (https://github.com/junming4)
 * @link       https://github.com/junming4
 */

return [
    'wapDomainList' => [
        'wxd1f5ff5269c86104' => [
            'articleVideo' => 'http://wap.howtool.cn',
            'default' => 'http://wap.howtool.cn',
        ],
        'default' => [
            'articleVideo' => 'http://wap.howtool.cn',
            'default' => 'http://wap.howtool.cn',
        ],
    ],
    'accountShare' => [
        'articleVideo' => [
            'domain'=> 'http://wechat.howtool.cn/open/article-video/',
            'appId' => 'wxd1f5ff5269c86104',
            'isNew' => false,
        ],
        'mNight' => [
            'domain' => 'http://wechat.howtool.cn/open/mnight/',
            'appId' => 'wxd1f5ff5269c86104',
            'isNew' => true
        ],
        'product' => [
            'domain' => 'http://wechat.howtool.cn/open/product/',
            'appId' => 'wxd1f5ff5269c86104',
            'isNew' => true
        ],
        'channel' => [
            'domain' => 'http://wechat.howtool.cn/open/channel/',
            'appId' => 'wxd1f5ff5269c86104',
            'isNew' => true
        ]
    ],
    'settingDomain' => [ //appid分享对应的域名，静态文章域名
        'wxd1f5ff5269c86104'=> 'http://wap.howtool.cn',
    ],
    'defaultUserId' => 41,
];
