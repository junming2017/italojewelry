<?php
declare(strict_types=1);
/**
 * PHP version 5.5
 *
 * @copyright  Copyright (c) 2012-2015 EELLY Inc. (https://github.com/junming4)
 * @link       https://github.com/junming4
 */

return [
    /*
     * 公众号
     */
    'official_account' => [
        'default' => [
            'app_id' => env('WECHAT_OFFICIAL_ACCOUNT_APPID', 'wxd1f5ff5269c86104'),// AppID
            'secret' =>  env('WECHAT_OFFICIAL_ACCOUNT_SECRET', '5578e81f513ab694586e4f6311cf8071'),
            'token'  => env('WECHAT_OFFICIAL_ACCOUNT_TOKEN', 'ab694586e4'),           // Token
            'aes_key' => env('WECHAT_OFFICIAL_ACCOUNT_AES_KEY', 'PNLBcQba2aKBrFpVqDdwJ6M4QF1QYjSBmAHJbXZ6gmU'),                 // EncodingAESKey
            'base_url' => 'https://api.weixin.qq.com',

            /*
             * OAuth 配置
             *
             * scopes：公众平台（snsapi_userinfo / snsapi_base），开放平台：snsapi_login
             * callback：OAuth授权完成后的回调页地址(如果使用中间件，则随便填写。。。)
             */
            // 'oauth' => [
            //     'scopes'   => array_map('trim', explode(',', env('WECHAT_OFFICIAL_ACCOUNT_OAUTH_SCOPES', 'snsapi_userinfo'))),
            //     'callback' => env('WECHAT_OFFICIAL_ACCOUNT_OAUTH_CALLBACK', '/examples/oauth_callback.php'),
            // ],
        ],
        'wxd1f5ff5269c86104' => [
            'app_id' => 'wxd1f5ff5269c86104',         // AppID
            'secret' => '5578e81f513ab694586e4f6311cf8071',
            'token' => 'ab694586e4',           // Token
            'aes_key' => 'PNLBcQba2aKBrFpVqDdwJ6M4QF1QYjSBmAHJbXZ6gmU',                 // EncodingAESKey
            'base_url' => 'https://api.weixin.qq.com',
        ],
    ],

    /*
     * 微信支付
     */
    'payment' => [
        'default' => [
            'sandbox' => env('WECHAT_PAYMENT_SANDBOX', false),
            'app_id' => env('WECHAT_PAYMENT_APPID', ''),
            'mch_id' => env('WECHAT_PAYMENT_MCH_ID', ''),
            'key' => env('WECHAT_PAYMENT_KEY', ''),
            'cert_path' => env('WECHAT_PAYMENT_CERT_PATH', config_path('key/apiclient_cert.pem')),    // XXX: 绝对路径！！！！
            'key_path' => env('WECHAT_PAYMENT_KEY_PATH', config_path('key/apiclient_key.pem')),      // XXX: 绝对路径！！！！
            'notify_url' => 'http://wechat.howtool.cn/api/payment/wechat-notify',                           // 默认支付结果通知地址
        ],
    ],
];
