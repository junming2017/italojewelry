<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'oss'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'url' => env('AWS_URL'),
        ],
        'oss' => [
            'driver'        => 'oss',
            'access_id'     => env('OSS_ACCESS_KEY_ID','LTAI4G7wyjNE4vqAMcVyM3QG'), //key,
            'access_key'    => env('OSS_SECRET_ACCESS_KEY','vnPx6y7qhFqfhNYV4n8ZN494lwDqjX'),//密钥,
            'bucket'        => env('OSS_BUCKET','kaidanyz'),//创建的bucket,
            'endpoint'      => env('OSS_DEFAULT_ENDPOINT','oss-cn-shenzhen.aliyuncs.com'),//'oss-cn-shenzhen.aliyuncs.com', // OSS 外网节点或自定义外部域名
            'endpoint_internal' => env('OSS_ENDPOINT_INTERNAL','kaidanyz.oss-cn-shenzhen-internal.aliyuncs.com'),//'kaidanyz.oss-cn-shenzhen-internal.aliyuncs.com',// '<internal endpoint [OSS内网节点] 如：oss-cn-shenzhen-internal.aliyuncs.com>', // v2.0.4 新增配置属性，如果为空，则默认使用 endpoint 配置(由于内网上传有点小问题未解决，请大家暂时不要使用内网节点上传，正在与阿里技术沟通中)
            'cdnDomain'     => env('OSS_CDN_DOMAIN','image-kaidanyz.oss-cn-shenzhen.aliyuncs.com'),//'image.kaidanyz.top',  // 如果isCName为true, getUrl会判断cdnDomain是否设定来决定返回的url，如果cdnDomain未设置，则使用endpoint来生成url，否则使用cdn
            'cdnRealDomain'     => env('OSS_CDN_REAL_DOMAIN','image-kaidanyz.kaidanyz.top'),//'image.kaidanyz.top',  // 如果isCName为true, getUrl会判断cdnDomain是否设定来决定返回的url，如果cdnDomain未设置，则使用endpoint来生成url，否则使用cdn
            'ssl'           => env('OSS_SSL',false), // true to use 'https://' and false to use 'http://'. default is false,
            'isCName'       => env('OSS_IS_CNAME',true),// 是否使用自定义域名,true: 则Storage.url()会使用自定义的cdn或域名生成文件url， false: 则使用外部节点生成url
            'debug'         => env('OSS_DEBUG',false),//是否开启调试模式
        ],
        'download' => [
            'driver'        => 'oss',
            'access_id'     => env('DOWNLOAD_ACCESS_KEY_ID','LTAI4G7wyjNE4vqAMcVyM3QG'), //key,
            'access_key'    => env('DOWNLOAD_SECRET_ACCESS_KEY','vnPx6y7qhFqfhNYV4n8ZN494lwDqjX'),//密钥,
            'bucket'        => env('DOWNLOAD_BUCKET','download-baoxuan'),//创建的bucket,
            'endpoint'      => env('DOWNLOAD_DEFAULT_ENDPOINT','oss-cn-shenzhen.aliyuncs.com'),//'oss-cn-shenzhen.aliyuncs.com', // OSS 外网节点或自定义外部域名
            'endpoint_internal' => env('DOWNLOAD_ENDPOINT_INTERNAL','download-baoxuan.oss-cn-shenzhen-internal.aliyuncs.com'),//'kaidanyz.oss-cn-shenzhen-internal.aliyuncs.com',// '<internal endpoint [OSS内网节点] 如：oss-cn-shenzhen-internal.aliyuncs.com>', // v2.0.4 新增配置属性，如果为空，则默认使用 endpoint 配置(由于内网上传有点小问题未解决，请大家暂时不要使用内网节点上传，正在与阿里技术沟通中)
            'cdnDomain'     => env('DOWNLOAD_CDN_DOMAIN','download-baoxuan.oss-cn-shenzhen.aliyuncs.com'),//'image.kaidanyz.top',  // 如果isCName为true, getUrl会判断cdnDomain是否设定来决定返回的url，如果cdnDomain未设置，则使用endpoint来生成url，否则使用cdn
            'cdnRealDomain' => env('DOWNLOAD_CDN_REAL_DOMAIN','download.dan123.top'),
            'ssl'           => env('DOWNLOAD_SSL',false), // true to use 'https://' and false to use 'http://'. default is false,
            'isCName'       => env('DOWNLOAD_IS_CNAME',true),// 是否使用自定义域名,true: 则Storage.url()会使用自定义的cdn或域名生成文件url， false: 则使用外部节点生成url
            'debug'         => env('DOWNLOAD_DEBUG',false),//是否开启调试模式
        ],
    ],

];
