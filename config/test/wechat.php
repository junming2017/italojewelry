<?php
declare(strict_types=1);
/**
 * PHP version 5.5
 *
 * @copyright  Copyright (c) 2012-2015 EELLY Inc. (https://github.com/junming4)
 * @link       https://github.com/junming4
 */

return [
    /*
     * 公众号
     */
    'official_account' => [
        'default' => [
            'app_id' => env('WECHAT_OFFICIAL_ACCOUNT_APPID','wx1062854dd7b2dbd6'),// AppID
            'secret' =>  'bc623d22ecaa944ca4e7fdd5ef79f16b',
            'token'  => '0HCMgo',           // Token
            'aes_key' => '',                 // EncodingAESKey
            'base_url' => 'https://api.weixin.qq.com',

            /*
             * OAuth 配置
             *
             * scopes：公众平台（snsapi_userinfo / snsapi_base），开放平台：snsapi_login
             * callback：OAuth授权完成后的回调页地址(如果使用中间件，则随便填写。。。)
             */
            // 'oauth' => [
            //     'scopes'   => array_map('trim', explode(',', env('WECHAT_OFFICIAL_ACCOUNT_OAUTH_SCOPES', 'snsapi_userinfo'))),
            //     'callback' => env('WECHAT_OFFICIAL_ACCOUNT_OAUTH_CALLBACK', '/examples/oauth_callback.php'),
            // ],
        ],
        'wx1062854dd7b2dbd6' => [
            'app_id' => 'wx1062854dd7b2dbd6',         // AppID
            'secret' => 'bc623d22ecaa944ca4e7fdd5ef79f16b',
            'token' => '0HCMgo',           // Token
            'aes_key' => '',                 // EncodingAESKey
            'base_url' => 'https://api.weixin.qq.com',
        ],
    ],

    /*
     * 微信支付
     */
    'payment' => [
        'default' => [
            'sandbox' => env('WECHAT_PAYMENT_SANDBOX', false),
            'app_id' => env('WECHAT_PAYMENT_APPID', ''),
            'mch_id' => env('WECHAT_PAYMENT_MCH_ID', ''),
            'key' => env('WECHAT_PAYMENT_KEY', ''),
            'cert_path' => env('WECHAT_PAYMENT_CERT_PATH', config_path('key/apiclient_cert.pem')),    // XXX: 绝对路径！！！！
            'key_path' => env('WECHAT_PAYMENT_KEY_PATH', config_path('key/apiclient_key.pem')),      // XXX: 绝对路径！！！！
            'notify_url' => 'http://wechat.howtool.cn/api/payment/wechat-notify',                           // 默认支付结果通知地址
        ],
    ],
];
