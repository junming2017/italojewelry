<?php
declare(strict_types=1);
/**
 * PHP version 5.5
 *
 * @copyright  Copyright (c) 2012-2015 EELLY Inc. (https://github.com/junming4)
 * @link       https://github.com/junming4
 */

return [
    'wapDomainList' => [
        'wx1062854dd7b2dbd6' => [
            'articleVideo' => 'http://127.0.0.1:8080',
        ],
        'default' => [
            'articleVideo' => 'http://127.0.0.1:8080',
        ],
    ],
    'settingDomain' => [ //appid分享对应的域名，静态文章域名
        'wx1062854dd7b2dbd6'=> 'http://127.0.0.1:8080',
    ],
    'accountShare' => [
        'articleVideo' => [
            'domain'=> 'http://127.0.0.1:8000/open/article-video/',
            'appId' => 'wx1062854dd7b2dbd6',
            'isNew' => false,
        ],
        'product' => [
            'domain' => 'http://127.0.0.1:8000/open/product/',
            'appId' => 'wx1062854dd7b2dbd6',
            'isNew' => true
        ],
        'channel' => [
            'domain' => 'http://127.0.0.1:8000/open/channel/',
            'appId' => 'wx1062854dd7b2dbd6',
            'isNew' => true
        ]
    ],
    'defaultUserId' => 35,
];
