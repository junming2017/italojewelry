<?php
declare(strict_types=1);
/**
 * PHP version 5.5
 *
 * @copyright  Copyright (c) 2012-2015 EELLY Inc. (https://github.com/junming4)
 * @link       https://github.com/junming4
 */


$domain = require_once (base_path('/config/'.env('APP_ENV','production').'/domain_setting.php'));


$common =  [
    //文件上传属性
    'fileUploadAttributes' => [
        'maxSize' => 1073741824, //1G
        'supportFormat' => [ //支持格式
            'music' => ['mp3'], //音频支持
            //视频支持
            'video' => [
                "flv", "swf", "mkv", "avi", "rm", "rmvb", "mpeg", "mpg",
                "ogg", "ogv", "mov", "wmv", "mp4", "webm", "mp3", "wav", "mid",
            ],
            'picture' => [
                'gif', 'jpg', 'bmp', 'png', 'jpeg'
            ],
            'folder' => [
                'pdf','doc','docx','ppt','pptx'
            ]
        ],
        //所有模块，对媒体文件进行分区
        'allModule' => ['default', 'media']
    ],
    'appName' => '微保典管理后台',
    'withdrawType' => [
        [
            'id' => 11,
            'name' => '银行卡'
        ], [
            'id' => 1,
            'name' => '支付宝'
        ], [
            'id' => 2,
            'name' => '微信号'
        ],
    ],
    'allowVideoUrl' => [
        'douYin' => [
            'v.douyin.com',
            'douyin.com',
            'www.iesdouyin.com',
            'iesdouyin.com',
        ],
        'kuaiShou' => [
            'm.chenzhongtech.com',
            'kuaishou.com',
            'f.kuaishou.com',
            'v.kuaishou.com',
        ],
        'other' => [
            'u.hkvv.cn',
        ]
    ],
    'allowArticleUrl' => [
        'weixin' => [
            'mp.weixin.qq.com'
        ],
    ],
    'sensitiveWordLevels' => [
        [
            'id' => 1,
            'level' => '一般',
        ],
        [
            'id' => 2,
            'level' => '严重'
        ]
    ],
    'bankListCommon' => [
        [
            'type' => 2,
            'imageUrl' => 'https://tstxx.oss-cn-beijing.aliyuncs.com/img/%E5%BE%AE%E4%BF%A1%E6%96%B9.jpg',
            'name' => '微信'
        ],
        [
            'type' => 1,
            'imageUrl' => 'https://tstxx.oss-cn-beijing.aliyuncs.com/img/%E6%94%AF%E4%BB%98%E5%AE%9D%E6%96%B9.jpg',
            'name' => '支付宝',
        ],
    ],
    'priceSettingUnit'=> [
        [
            'name' => '月',
            'id' => 1,
            'value' => 30
        ],
        [
            'name' => '年',
            'id' => 2,
            'value' => 365
        ],
    ],
    'defaultSignature' => '您好，我在保险公司从业多年，熟悉各类保险公司险种！保险就是给家庭财富穿上防弹衣，让家庭免于意外伤害。为了您和家人的幸福，建议您购买充足的保障！如果您在保险方面有疑问，可拨打电话联系我！',
    'dyKsOptions' => [
        [
            'id' => 1,
            'name' => '抖音用户',
        ],
        [
            'id' => 2,
            'name' => '抖音频道'
        ]
    ],
    'unlimitedUser' => [
        1,2,3
    ],
    'randCopyWriting'=> [
        [
            'title' => '%d个客户看了你的文章',
            'remark' => '不能错过这样的机会啊...'
        ],
        [
            'title' => '已有%d人看了你的文章',
            'remark' => '你还说没有客户资源吗？'
        ],
        [
            'title' => '你已错过%d次跟进机会',
            'remark' => '有多少机会可以继续错过？'
        ]
    ],
    'customerItemLists' => [ //客户轮播
        [
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E5%88%9A%E5%93%A5.jpg',
                'text' => '上周锁定了<span class="info_num">15</span>个客户',
                'name' => '刚哥'
            ],
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E5%B9%B3%E5%AE%89%E7%94%B0%E7%BB%8F%E7%90%86.jpg',
                'text' => '正在追踪客户',
                'name' => '平安田经理'
            ],
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E5%BB%BA%E7%B2%A4%E8%A3%85%E9%A5%B0%20%E5%85%A8%E5%B1%8B%E5%AE%9A%E5%88%B6.jpg',
                'text' => '<span class="info_num">3</span>分钟前添加了一个客户',
                'name' => '建粤装饰 全屋定制'
            ],
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E6%88%91%E7%88%B1%E6%BC%94%E8%AE%B2-%E5%B0%8F%E4%B9%9F.jpg',
                'text' => '刚刚追踪到一个客户',
                'name' => '我爱演讲-小也'
            ],
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E7%A7%80%E6%83%A0%EF%BD%9C%E6%98%8E%E4%BA%9A%E4%BF%9D%E9%99%A9%E7%BB%8F%E7%BA%AA.jpg',
                'text' => '上周添加了<span class="info_num">8</span>个客户',
                'name' => '秀惠｜明亚保险经纪'
            ],
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E8%83%A1%E5%BF%97%E8%8A%B915102783031.jpg',
                'text' => '今日已获取<span class="info_num">5</span>个客户',
                'name' => '胡志芹15102783031'
            ]
        ],
        [
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E7%88%86%E5%93%81%E7%8E%8B.jpg',
                'text' => '上周跟进客户<span class="info_num">325</span>次',
                'name' => '爆品王'
            ],
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E5%81%A5%E5%BA%B7%E8%90%A5%E5%85%BB%E5%B8%88%E9%82%A2%E5%BD%A6%E6%96%8C.jpg',
                'text' => '正在跟进客户',
                'name' => '健康营养师邢彦斌'
            ],
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E9%9D%99%E9%9B%85%E5%B7%A5%E4%BD%9C%E5%AE%A4.jpg',
                'text' => '<span class="info_num">6</span>分钟前添加了一个客户',
                'name' => '静雅工作室'
            ],
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E7%90%86%E8%B4%A2%E8%A7%84%E5%88%92%E5%B8%88%E8%A4%9A%E5%A8%9C%E5%A8%9C.jpg',
                'text' => '刚刚追踪到一个客户',
                'name' => '理财规划师褚娜娜'
            ],
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E5%A4%A9%E6%B4%A5%E7%9B%8A%E5%80%8D%E7%8E%8B%E6%B5%B7%E7%87%95.jpg',
                'text' => '上周销售成交<span class="info_num">19</span>单',
                'name' => '天津益倍王海燕'
            ],
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/CMRH-%E5%88%98%E6%B9%98%E5%8D%8E.jpg',
                'text' => '今日跟进客户<span class="info_num">47</span>次',
                'name' => 'CMRH-刘湘华'
            ],
        ],
        [ //第三组
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E8%94%A1%E5%8D%9A%E6%B5%A9%20Nick.jpg',
                'text' => '上周跟进客户<span class="info_num">493</span>次',
                'name' => '蔡博浩 Nick'
            ],
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E8%BE%B0%E9%A2%90%E7%89%A9%E8%AF%AD%E6%80%BB%E4%BB%A3.jpg',
                'text' => '正在跟进客户',
                'name' => '辰颐物语总代'
            ],
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E6%AE%B5%E4%BC%9F.jpg',
                'text' => '<span class="info_num">10</span>分钟前添加了一个客户',
                'name' => '段伟'
            ],
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E6%B3%9B%E5%8D%8E%E4%BF%9D%E9%99%A9%E6%80%BB%E7%9B%91%E5%88%98%E7%87%95.jpg',
                'text' => '刚刚追踪到一个客户',
                'name' => '泛华保险总监刘燕'
            ],
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E8%8E%B1%E6%81%A9%E6%9D%A8%E6%B4%8B15596648133.jpg',
                'text' => '上周销售成交<span class="info_num">15</span>单',
                'name' => '莱恩杨洋15596648133'
            ],
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E8%B7%AF%E8%B7%AF%7E15824600085.jpg',
                'text' => '今日跟进客户<span class="info_num">39</span>次',
                'name' => '路路~15824600085'
            ]
        ],
        [ //第四组
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E5%AE%89%E8%8E%89%E5%A8%9F%20Ann.jpg',
                'text' => '上周锁定了<span class="info_num">12</span>个客户',
                'name' => '安莉娟 Ann'
            ],
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E8%91%86%E5%A9%B4-USANA%E7%8E%8B%E7%A7%80%E5%B9%B3.jpg',
                'text' => '正在追踪客户',
                'name' => '葆婴-USANA王秀平'
            ],
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E9%A3%8E%E9%99%A9%E7%AE%A1%E7%90%86%E5%B8%88%E6%A8%8A%E6%95%8F%E6%8D%B7.jpg',
                'text' => '<span class="info_num">7</span>分钟前添加了一个客户',
                'name' => '风险管理师樊敏捷'
            ],
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E9%AB%98%E7%BA%A7%E8%90%A5%E5%85%BB%E5%B8%88.jpg',
                'text' => '刚刚追踪到一个客户',
                'name' => '高级营养师'
            ],
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E5%AD%99%E6%88%90%E6%96%87.jpg',
                'text' => '上周添加了<span class="info_num">23</span>个客户',
                'name' => '孙成文'
            ],
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E7%9B%B4%E6%92%AD%E5%B8%A6%E8%B4%A7%E8%B5%A2%E5%A4%A9%E4%B8%8B.jpg',
                'text' => '上周销售成交<span class="info_num">9</span>单',
                'name' => '直播带货赢天下'
            ]
        ],
        [ //第5组
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E9%99%88%E8%B4%A4%E9%9B%B7%E7%90%86%E8%B4%A2%E5%B7%A5%E4%BD%9C%E5%AE%A4.jpg',
                'text' => '上周跟进客户<span class="info_num">186</span>次',
                'name' => '陈贤雷理财工作室'
            ],
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E7%A8%8B%E5%BF%B5%C2%B7%E4%BF%9D%E9%99%A9%E7%BB%8F%E7%BA%AA%E4%BA%BA.jpg',
                'text' => '正在跟进客户',
                'name' => '程念·保险经纪人'
            ],
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E5%B9%B3%E5%AE%89%E6%9D%8E%E7%BB%8D%E4%BE%A01338564962.jpg',
                'text' => '<span class="info_num">10</span>分钟前添加了一个客户',
                'name' => '平安李绍侠1338564962'
            ],
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E7%8E%8B%E5%A6%AE%C2%B7%E8%B4%A2%E5%AF%8C%E7%AE%A1%E7%90%86.jpg',
                'text' => '刚刚追踪到一个客户',
                'name' => '王妮·财富管理'
            ],
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E6%96%B0%E9%9D%92%E5%B9%B4%E8%B5%84%E6%9C%AC-%E5%90%B4%E6%B1%9F.jpg',
                'text' => '上周销售成交<span class="info_num">13</span>单',
                'name' => '新青年资本-吴江'
            ],
            [
                'imageUrl' => 'http://image.kaidanyz.top/customer/%E9%93%B6%E8%A1%8C%E8%B4%B7%E6%AC%BE-%E4%BE%AF%E4%BF%8A.jpg',
                'text' => '今日跟进客户<span class="info_num">25</span>次',
                'name' => '银行贷款-侯俊'
            ]
        ]
    ],
    'commentItems' => [ //评价
        [
            'imageUrl' => 'http://image.kaidanyz.top/comment/%E4%B8%A5%E5%BB%BA%E6%96%B0-%E4%B8%93%E4%B8%9A%E7%90%86%E8%B4%A2%E8%A7%84%E5%88%92%E5%B8%88.jpg',
            'text' => '非常棒！一直在寻找这样的平台，没有想到这就是我心中所想象中的那样，非常的省心，不用自己找保障案例，平台全部帮我准备好了。',
            'name' => '严建新-专业理财规划师'
        ],
        [
            'imageUrl' => 'http://image.kaidanyz.top/comment/%E5%BC%A0%E4%B9%BE%E5%9D%A4-%E9%AB%98%E7%BA%A7%E8%90%A5%E5%85%BB%E5%B8%88.jpg',
            'text' => '真是个好工具啊，知道了客户意向，一下就找到了沟通话题，客户觉得我特别了解她，真开心，我会继续使用的。',
            'name' => '张乾坤-高级营养师'
        ],
        [
            'imageUrl' => 'http://image.kaidanyz.top/comment/%E6%9D%A8%E9%A2%96-%E4%B8%AD%E5%9B%BD%E5%B9%B3%E5%AE%89.jpg',
            'text' => '因为平安小伙伴的推荐，就开了会员。后来真的帮我找到了不少意向客户，这是很令我意外的。非常感谢你们！',
            'name' => '杨颖-中国平安'
        ],
        [
            'imageUrl' => 'http://image.kaidanyz.top/comment/Wing-%E5%9B%A2%E7%88%86%E5%93%81.jpg',
            'text' => '再也不愁发什么内容了，随便选一篇好文章好视频，这一点我很喜欢，还可以插入广告，已经推荐团队里的人全在用了，非常省心。',
            'name' => 'wing-京东芬香'
        ],
        [
            'imageUrl' => 'http://image.kaidanyz.top/comment/%E6%9D%8E%E5%B3%A5-%E6%B3%B0%E5%BA%B7%E4%BA%BA%E5%AF%BF%E4%B8%9A%E5%8A%A1%E6%80%BB%E7%9B%91.jpg',
            'text' => '这个平台真是太神奇了，帮我挖出来好几个意向客户了，点赞点赞，让泰康的伙伴们都在用了~',
            'name' => '李峥-泰康人寿业务总监'
        ],
        [
            'imageUrl' => 'http://image.kaidanyz.top/comment/%E6%9D%8E%E4%B8%B9%E9%9C%9E-%E5%AE%89%E5%88%A9%E5%88%9B%E5%AE%A2.jpg',
            'text' => '嘿嘿，昨天卖了1万多，特别感谢你们。这平台太伟大了，帮助我找了不少客户，真心感谢、支持，祝愿你们越办越好！',
            'name' => '李丹霞-安利创客'
        ],
        [
            'imageUrl' => 'http://image.kaidanyz.top/comment/%E5%BD%A9%E4%BA%91-%E7%A4%BE%E4%BA%A4%E6%96%B0%E9%9B%B6%E5%94%AE.jpg',
            'text' => '我刚学习发文，对网络不懂，这里有好多文章和学习资料，平台的指导老师也特别耐心，帮助我成长，谢谢，今天有好几个人看了我的文章，正在学习怎么跟进他们。',
            'name' => '彩云-淘小铺'
        ],
        [
            'imageUrl' => 'http://image.kaidanyz.top/comment/%E8%91%A3%E5%B0%8F%E5%A7%90-%E5%AE%9A%E5%88%B6%E6%97%85%E6%B8%B8.jpg',
            'text' => '文章特别多，有很多好文章，游客们咨询多了起来，还来转发我的文章，深感好用，感谢这个平台。',
            'name'=>'董小姐-定制旅游',
        ]
    ]
];

return array_merge($common,$domain);
