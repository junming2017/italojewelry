const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig({
    plugins: [
    ],
    externals: {//配置不打包的选项
        'element-ui': 'Element',
        'vue': 'Vue',
        'vuex': 'Vuex',
        'vue-router': 'VueRouter',
        'vue-chartjs': 'VueChartJs',
        'jquery' : 'jQuery',
    } });


mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');

/*const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;*/

/*const { CleanWebpackPlugin } = require('clean-webpack-plugin');*/

/*mix.webpackConfig({
    plugins:[
        new BundleAnalyzerPlugin(),
    ]
});*/

/*mix.webpackConfig({
    externals: {
        'element-ui': 'ELEMENT',//这个比较坑　一开始我还以为是ElementUI结果就报错了XD
    }
});*/

