<!--左侧导航-->
<aside class="lyear-layout-sidebar">

    <!-- logo -->
    <div id="logo" class="sidebar-header">
        <a href="{{ route('admin.dashboard') }}"><img src="{{ asset('backend/images/logo-sidebar.png') }}" title="蜗牛团餐" alt="蜗牛团餐"/></a>
    </div>
    <div class="lyear-layout-sidebar-scroll">

        <nav class="sidebar-main">
            <ul class="nav nav-drawer">
                <li class="nav-item @if(Request::is('admin')) active @endif">
                    <a href="{{ route('admin.dashboard') }}"><i class="mdi mdi-home"></i>日常运营</a>
                </li>


                {{--图片管理--}}
                <li class="nav-item nav-item-has-subnav @if(Request::is('admin/image/*')) active open @endif">
                    <a href="javascript:void(0)"><i class="mdi mdi-folder-image"></i>图片系统</a>
                    <ul class="nav nav-subnav">
                        <li @if(Request::is('admin/image/index')) class="active" @endif>
                            <a href="{{ route('admin.image.index') }}">图片管理</a>
                        </li>
                    </ul>
                </li>


            </ul>
        </nav>
        <div class="sidebar-footer" style="margin-top:150px;color: #d1caca;">
            <p class="copyright">Copyright &copy; 2021. Woniu All rights reserved. </p>
        </div>
    </div>

</aside>
<!--End 左侧导航-->
