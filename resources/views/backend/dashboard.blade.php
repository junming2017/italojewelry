@extends('backend.layouts.master')
@section('title','日常运营')

@section('content')
    <div id="app">
        <dashboard-component></dashboard-component>
    </div>
@endsection

@section('scripts')
    <script src="{{ mix('js/app.js') }}"></script>
@endsection
