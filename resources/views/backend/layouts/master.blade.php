<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
    <title>@yield('title') - {{ config('common.appName') }}</title>
    <link rel="icon" href="/favicon.ico" type="image/ico">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="yinqi">
    <link href="{{ asset('backend/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('backend/css/swiper.min.css') }}">
    <link href="{{ asset('backend/css/materialdesignicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/style.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/element-ui@2.13.1/lib/theme-chalk/index.css">
    <script src="{{ asset('backend/js/jquery.min.js') }}"></script>
    <script src="{{ asset('backend/js/swiper.min.js') }}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="id" content="@yield('id','')">

    @yield('styles')
</head>

<body>
<div class="lyear-layout-web">
    <div class="lyear-layout-container">
    @include('backend.partials._sidebar')
    @include('backend.partials._header')

    <!--页面主要内容-->
        <main class="lyear-layout-content">

            @yield('content')

        </main>
        <!--End 页面主要内容-->

    </div>
</div><!-- /.main-container -->


{{--<script type="text/javascript" src="{{ asset('backend/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/bootstrap.min.js') }}"></script>--}}
<script type="text/javascript" src="{{ asset('backend/js/perfect-scrollbar.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('backend/js/main.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.11"></script>
<script src="https://unpkg.com/element-ui@2.13.1/lib/index.js"></script>

@yield('scripts')
<!--图表插件-->
</body>
</html>
