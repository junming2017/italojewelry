@extends('backend.layouts.master')

@section('title','图片列表')

@section('content')
    <div id="app">
        <image-component></image-component>
    </div>
@endsection
@section('scripts')
    <script src="{{ mix('js/app.js') }}"></script>
    <!--时间选择插件-->
@endsection
