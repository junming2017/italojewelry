@extends('backend.layouts.master')

@section('title','图片显示')

@section('id',$id)

@section('content')
    <div id="app">
        <image-detail-component></image-detail-component>
    </div>
@endsection
@section('scripts')
    <script src="{{ mix('js/app.js') }}"></script>
    <!--时间选择插件-->
@endsection
