import axios from "../plugins/axios";

export default  {

    getEndTime: function (date) {
        this.endTime = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
    },
    getStartTime(date) {
        this.startTime = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
    },
    indexMethod: function (index) {
        return index + 1;
    },
    formatTitle: function (row, column) {
        return this.filterLength(row.title, 10);
    },
    formatCompany: function (row, column) {
        return this.filterLength(row.companyStr, 4);
    },
    formatIndustry: function (row, column) {
        return this.filterLength(row.categoryStr, 4);
    },
    formatStatus: function (row, column) {
        return row.status === 0 ? '隐藏' : '显示';
    },
    deleteRequireArticleVideoCartoon: function (id) {
        let self = this;
        let uri = "/article/video/cartoon/" + id;
        axios.delete(uri).then(function (result) {
            self.search();
        },function (error) {
            self.loading = false;
        });
    },

    /*分类和公司*/
    async querySearchAsync(queryString, cb) {
        this.queryIndustry(queryString);
        var restaurants = this.restaurants;
        var results = queryString ? restaurants.filter(this.createStateFilter(queryString)) : restaurants;

        clearTimeout(this.timeout);
        this.timeout = setTimeout(() => {
            cb(results);
        }, 3000 * Math.random());

    },
    handleQuerySelect(item) {
        if (item && this.categoryIds.indexOf(item.id) === -1) {
            this.categoryTags.push(item);
            this.categoryIds.push(item.id);
        }
        this.form.categoryName = '';
    },
    async queryCompanyAsync(queryString, cb) {
        this.queryCompany(queryString);
        var restaurants = this.companies;
        var results = queryString ? restaurants.filter(this.createStateFilter(queryString)) : restaurants;
        clearTimeout(this.timeout);
        this.timeout = setTimeout(() => {
            cb(results);
        }, 1000 * Math.random());
    },
    handleCompanySelect(item) {
        if (item && this.companyIds.indexOf(item.id) === -1) {
            this.companyTags.push(item);
            this.companyIds.push(item.id);
        }
        this.form.companyName = '';
    },
    handleIndustryClose(tag) {
        this.categoryTags.splice(this.categoryTags.indexOf(tag), 1);
        this.categoryIds.splice(this.categoryIds.indexOf(tag.id), 1);
    },
    handleCompanyClose(tag) {
        this.companyTags.splice(this.companyTags.indexOf(tag), 1);
        this.companyIds.splice(this.companyIds.indexOf(tag.id), 1);
    },
    /*搜索部分*/
    handleSizeChange: function (pageSize) {
        this.pageSize = pageSize;
        this.search();
    },
    handleCurrentChange: function (val) {
        this.currentPage = val;
        this.search();
    },
    searchClick() {
        this.currentPage = 1;
        this.search();
    },
    sortChange(item) {
        let order = "asc";
        this.orderField = item.prop;
        if (item.order === 'descending') {
            order = "desc";
        }
        this.order = order;
        this.search();
    },

    dealQueryCompany(keyword = '') {
        return new Promise((resolve, reject) => {
            var uri = "company/search/" + keyword;
            axios.get(uri).then(function (response) {
                let data = response.list;
                resolve(data);
            });
        })
    },
    queryCompany(queryString = ''){
        let self = this;
        this.dealQueryCompany(queryString).then(res => {
            self.companies = res;
        });
    },
    dealQueryIndustry(keyword = "") { //获取分享账号
        return new Promise((resolve, reject) => {
            var uri = "category/search/" + keyword;
            axios.get(uri).then(function (response) {
                let data = response.list;
                resolve(data);
            });
        })
    },
    queryIndustry(queryString = ''){
        let self = this;
        this.dealQueryIndustry(queryString).then(res => {
            self.restaurants = res;
        });
    },
    createStateFilter(queryString) {
        return (state) => {
            return (state.value.toLowerCase().indexOf(queryString.toLowerCase()) >-1);
        }
    },
    openUri(uri) {
        window.location.href = '/admin/'+uri;
    },
    playerInit:function() {
        this.player =  this.$video(myVideo, {
            //确定播放器是否具有用户可以与之交互的控件。没有控件，启动视频播放的唯一方法是使用autoplay属性或通过Player API。
            // controls: true,
            //自动播放属性,muted:静音播放
            //autoplay: "muted",
            //建议浏览器是否应在<video>加载元素后立即开始下载视频数据。
            //preload: "auto",
            //设置视频播放器的显示宽度（以像素为单位）
            width: "250px",
            //设置视频播放器的显示高度（以像素为单位）
            height: "250px",

            fullScreen: {
                controlBar: true
            }
        });
    },
    beforeAvatarUpload: function (item) {
        console.log(item)
    },
    beforeFileUpload: function (item) {
        this.loading = true;
    },
    handleFileSuccess: function (response) {
        let self = this;
        this.loading = false;
        if (response.code === 200) {
            self.$message.success("上传成功！");
            self.form.videoUrl = response.url;
        }
    },

}
