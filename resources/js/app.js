
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/*import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);*/

import VueQuillEditor from 'vue-quill-editor'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
Vue.use(VueQuillEditor);
import {quillRedefine} from 'vue-quill-editor-upload'
Vue.use(quillRedefine);
//https://github.com/NextBoy/quill-image-extend-module 新插件
import VueUeditorWrap from 'vue-ueditor-wrap'
Vue.component('vue-ueditor-wrap', VueUeditorWrap);

import VueDND from 'awe-dnd'
Vue.use(VueDND);

import Video from 'video.js'
import 'video.js/dist/video-js.css'

Vue.prototype.$video = Video;
/*Vue.use(Video);*/

import Vue from 'vue'

import axios from './plugins/axios'
Vue.prototype.$axios = axios;

console.log(process.env);

const VUE_APP_BASE_URL = '';

axios.defaults.baseURL = VUE_APP_BASE_URL;  //关键代码

var instance = axios.create({
    headers: {
        'Content-Type': 'multipart/form-data'
    }
});
Vue.prototype.instance = instance;


Vue.use(require('vue-moment'));

Vue.component('login-component', require('./components/backend/LoginComponent.vue'));
Vue.component('dashboard-component', require('./components/backend/DashboardComponent.vue'));
//

//图片列表
Vue.component('image-component',require('./components/backend/ImageComponent.vue'));
Vue.component('image-detail-component',require('./components/backend/ImageDetailComponent.vue'));

const app = new Vue({
    el: '#app'
});
