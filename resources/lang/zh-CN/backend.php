<?php
declare(strict_types=1);

/**
 * PHP version 5.5
 *
 * @copyright  Copyright (c) 2012-2015 EELLY Inc. (https://github.com/junming4)
 * @link       https://github.com/junming4
 */

return [
    'attributes' => [
        'home'        => '主页',
        'confirm'     => '确认',
        'manage'      => '管理',
        'list'        => '列表',
        'create'      => '添加',
        'edit'        => '编辑',
        'update'      => '更新',
        'fail'        => '失败',
        'notFund'     => '数据没有找到',
        'success'     => '成功',
        'createdAt'   => '创建时间',
        'cannot'      => '不能',
        'option'      => '选择',
    ],
];
