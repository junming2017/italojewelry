$(function () {

    $(".remove-btn").click(function () {
        var url = $(this).attr('data-url');


        layer.confirm('您是确定删除该数据？', {
            btn: ['确定','取消'] //按钮
        }, function(){
            RYD.GLOBALS.rydajax(url,'delete', {'_token': _token}, 'json',function (data) {
                //$(this).parents('tr').remove();
                layer.msg('的确很重要', {icon: 1});
                window.location.reload();
            });

        });

    });

    /*layer.confirm('您是如何看待前端开发？', {
        btn: ['重要','奇葩'] //按钮
    }, function(){
        layer.msg('的确很重要', {icon: 1});
    }, function(){
        layer.msg('也可以这样', {
            time: 20000, //20s后自动关闭
            btn: ['明白了', '知道了']
        });
    });*/

});
