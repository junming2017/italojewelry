var newsDelObj = null;

$(function () {

    $(".btnDel").click(function () {
        newsDelObj = $(this).parents('.deleteFrom');
        layer.open({
            type: 1,
            title: false,
            closeBtn: 0,
            area: ['auto', 'auto'],
            shadeClose: true,
            skin: 'layui-news_box',
            content: '<div class="m-tk-del m-tk-o2">' +
            '<div class="m-tk-top">　您确定要删除吗？</div>' +
            '<div class="m-tk-bt">' +
            '<a  href="javascript:void(0);" class="m-tk-cox">取消</a>' +
            '<a  href="javascript:void(0);" onclick="sendForm()">确定</a>' +
            '</div>' +
            '</div>'
        });
    });

    $('.m-tk-cox').click(
        layer.close()
    );

});
function sendForm() {
    newsDelObj.submit();
}
