var RYD = RYD || {};
var _token = $('meta[name="csrf-token"]').attr('content');
RYD.GLOBALS = {};
RYD.APPS = {};
RYD.APPS.loan = {};
RYD.GLOBALS.redirectUrl = '';
RYD.GLOBALS.gmsg = '';

RYD.GLOBALS.rydajax = function(url, method, params, dataType, callback)
{
    $.ajax({
        url: url,
        type: method,
        data: params,
        dataType: dataType,
        success: callback,
        error:function(error){
            var parsedJson = jQuery.parseJSON(error.responseText);
            if(parsedJson.error.msg){
                alert(parsedJson.error.msg);
            }else{
                alert('请求异常');
            }
        }
    });
};

RYD.GLOBALS.showHistoryMsg = function () {
    RYD.GLOBALS.showTopLeft(RYD.GLOBALS.gmsg , 'info');
};

// TOOD
RYD.GLOBALS.showInfo = function(data) {
    if (data.code === 200 || data.status === 'succeed') {
        //RYD.GLOBALS.gmsg = '正确信息' + data.message;
        RYD.GLOBALS.showBottomRight(data.msg , 'success');
        if(data.errormessage !== undefined && data.errormessage !== ''){
            RYD.GLOBALS.showBottomRight(data.errormessage , 'warning');
            //RYD.GLOBALS.gmsg = '警告信息' + data.errormessage;
        }
        if (RYD.GLOBALS.redirectUrl  !== '') {
            window.location.href = RYD.GLOBALS.redirectUrl ;
        } else {
            setTimeout(function () {
                history.go(0);
            },1000);
        }
    } else {
        RYD.GLOBALS.showBottomRight(data.error.message, 'warning');
        //RYD.GLOBALS.gmsg = '错误信息' + data.error.message;
    }

};

RYD.GLOBALS.showError = function() {
    RYD.GLOBALS.showBottomRight(" 请正确操作系统，谢谢合作！" , 'warning');
};

RYD.GLOBALS.initToastr = function () {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-bottom-right",
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "2000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
};

RYD.GLOBALS.showTopLeft = function (message , meg_type) {
    toastr.options = {"positionClass": "toast-top-left"};
    if(meg_type === 'success'){
        toastr.success(message);
    } else if(meg_type === 'error'){
        toastr.error(message);
    } else if(meg_type === 'warning'){
        toastr.warning(message);
    } else {
        toastr.info(message);
    }
};

RYD.GLOBALS.showBottomRight = function (message , meg_type) {
    RYD.GLOBALS.initToastr();
    if(meg_type === 'success'){
        toastr.success(message);
    } else if(meg_type === 'error'){
        toastr.error(message);
    } else if(meg_type === 'warning'){
        toastr.warning(message);
    } else {
        toastr.info(message);
    }
};

RYD.GLOBALS.renderInput = function (className , name , value, type ) {
    if (type === undefined) {
        return "<input class='"+className+"' name='"+name+"' type='text' value='"+value+"'>";
    }
    return "<input class='"+className+"' name='"+name+"' type='"+type+"' value='"+value+"'>";
};
