let utils = {};
let baseUrl = "/admin/";

/**
 * @param uri post请求地址
 * @param param 参数
 */
Vue.prototype.post = function (uri, param, callback, errorBack,showError=true) {
    axios.post(baseUrl+uri, param, {
        withCredentials: true
    }).then(function (response) {
        var result = response.data;
        callback && callback(result);
    }).catch(function (error) {
        //errorBack && errorBack(error)
        let data = error.response.data;
        if (showError ===true){
            if (data.error ) {
                Vue.prototype.$message({message: data.error.msg, center: true});
            } else {
                Vue.prototype.$message({message: error.response.statusText, center: true});
            }
        }
        errorBack && errorBack(error)
    });
};
/**
 * @param uri put
 * @param param 参数
 */
Vue.prototype.put = function (uri, param, callback, errorBack) {
    axios.put(baseUrl+uri, param, {
        withCredentials: true
    }).then(function (response) {
        var result = response.data;
        callback && callback(result);
    }).catch(function (error) {
        //Vue.ElementUI.message("xx");
        //errorBack && errorBack(error)
        let data = error.response.data;
        if (data.error) {
            Vue.prototype.$message.error(data.error.msg);
        } else {
            Vue.prototype.$message.error(error.response.statusText);
        }
        errorBack && errorBack(error)
    });
};
Vue.prototype.delete = function (uri, callback, errorBack) {
    axios.delete(baseUrl+uri, {
        withCredentials: true
    }).then(function (response) {
        var result = response.data;
        callback && callback(result);
    }).catch(function (error) {
        //errorBack && errorBack(error)
        let data = error.response.data;
        if (data.error) {
            Vue.prototype.$message.error(data.error.msg);
        } else {
            Vue.prototype.$message.error(error.response.statusText);
        }
        errorBack && errorBack(error)
    });
};

Vue.prototype.get = function (uri, callback, errorBack) {
    axios.get(baseUrl+uri, {
        withCredentials: true
    }).then(function (response) {
        var result = response.data;
        callback && callback(result);
    }).catch(function (error) {
        //errorBack && errorBack(error)
        let data = error.response.data;
        if (data.error) {
            Vue.prototype.$message.error(data.error.msg);
        } else {
            Vue.prototype.$message.error(error.response.statusText);
        }
        errorBack && errorBack(error)
    });
};
Vue.prototype.openUri = function(uri){
    window.location.href = baseUrl+uri;
};

Vue.prototype.createStateFilter = function(queryString) {
    return (state) => {
        return (state.value.toLowerCase().indexOf(queryString.toLowerCase()) >-1);
    }
};


Vue.prototype.filterLength = function (str, length = 9) {
    if (str.length >= length) {
        return str.substring(0, length) + '...';
    }
    return (str.length === 0 || !str) ? '--' : str;
};


