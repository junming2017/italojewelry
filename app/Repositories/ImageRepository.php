<?php
declare(strict_types=1);
/**
 * PHP version 5.5
 *
 * @copyright  Copyright (c) 2012-2015 EELLY Inc. (https://github.com/junming4)
 * @link       https://github.com/junming4
 */


namespace App\Repositories;



use App\Models\Image;

class ImageRepository
{
    /**
     * 搜索处理.
     *
     * @param string $keyword
     * @param int $status
     * @param array $options
     * @param int $limit
     * @param string $orderField
     * @param string $order
     * @return
     * @author 肖俊明<2284876299@qq.com>
     * @since 2020年03月09日
     */
    public function getList(array $options = [], $limit = 10, $orderField = 'id', $order = "desc")
    {

        $image = new Image();
        foreach ($options as $key => $value) {
            if ($key == 'keyword'){
                $image = $image->where(function ($query) use ($value) {
                    return $query->where('title', 'like', '%' . $value . '%');
                });
            }else{
                $image = $image->where($key, $value);
            }

        }
        return $image->orderBy($orderField,$order)
            ->paginate($limit);
    }

    /**
     * 通过id获取数据.
     *
     * @param $id
     * @return mixed
     * @author 肖俊明<2284876299@qq.com>
     * @since 2020年09月01日
     */
    public function getInfoById($id)
    {
        if (!empty($id)) {
            return Image::find($id);
        }
        return [];
    }

}
