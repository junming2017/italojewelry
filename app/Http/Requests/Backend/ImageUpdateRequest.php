<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class ImageUpdateRequest extends ImageCreateRequest
{
    /**
     * attributes.
     *
     * @return array
     * @author 肖俊明<2284876299@qq.com>
     * @since 2020年03月14日
     */
    public function attributes ()
    {
        return [
            'remark' => trim($this->remark),
        ];
    }
}
