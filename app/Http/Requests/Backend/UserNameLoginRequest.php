<?php

namespace App\Http\Requests\Backend;

use App\Http\Requests\ApiFormRequestBase;
use Illuminate\Foundation\Http\FormRequest;

class UserNameLoginRequest extends ApiFormRequestBase
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'     => 'required|string',
            'password' => 'required|string',
            'captcha'  => 'required|captcha'
        ];
    }


    public function messages ()
    {
        return  [
            'name.required'    => '必须填写“账号”',
            'password.required'  => '必须填写“密码”',
            'captcha.required'  => '必须填写“验证码”',
            'captcha.captcha'  => '请输入正确的“验证码”'
        ];
    }

    public function attributes ()
    {
        return [
            'name'     => $this->name,
            'password' => $this->password,
        ];
    }

}
