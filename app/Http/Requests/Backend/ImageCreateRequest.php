<?php

namespace App\Http\Requests\Backend;

use App\Http\Requests\ApiFormRequestBase;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ImageCreateRequest extends ApiFormRequestBase
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'remark'     => 'required|string|max:200',
            //'describe'  => 'required|string|max:250',
            //'imageUrl'  => 'required|string',
            //'body'      => 'required|string',
        ];
    }


    public function messages ()
    {
        return  [
            'remark.required'    => '必须填写“备注信息”',
        ];
    }

    public function attributes ()
    {
        return [
            'remark' => trim($this->remark),
        ];
    }
}
