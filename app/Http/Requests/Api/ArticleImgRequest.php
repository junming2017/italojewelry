<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\ApiFormRequestBase;
use Illuminate\Foundation\Http\FormRequest;

class ArticleImgRequest extends ApiFormRequestBase
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'imageUrl'  => 'required|string',
            'title'    => 'required|string',
        ];
    }

    public function messages ()
    {
        return  [
            'imageUrl.required' => '请上传封面图',
            'title.required' => '标题不能为空'
        ];
    }

    public function attributes()
    {
        return [
            'imageUrl' => $this->imageUrl,
            'title' => $this->title,
        ];
    }
}
