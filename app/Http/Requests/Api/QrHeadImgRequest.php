<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\ApiFormRequestBase;
use Illuminate\Foundation\Http\FormRequest;

class QrHeadImgRequest extends ApiFormRequestBase
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'imageUrl'  => 'required|string',
            'backgroundImg'    => 'required|string',
        ];
    }

    public function messages ()
    {
        return  [
            'imageUrl.required' => '请上传头像',
            'backgroundImg.required' => '背景图片不能为空',
        ];
    }

    public function attributes()
    {
        return [
            'imageUrl' => $this->imageUrl,
            'backgroundImg' => $this->backgroundImg,
        ];
    }
}
