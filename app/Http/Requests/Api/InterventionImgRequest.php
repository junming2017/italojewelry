<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\ApiFormRequestBase;
use Illuminate\Foundation\Http\FormRequest;

class InterventionImgRequest extends ApiFormRequestBase
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'imageUrl'  => 'required|string',
            'headerImg'    => 'required|string',
            'nickname'    => 'required|string',
        ];
    }

    public function messages ()
    {
        return  [
            'imageUrl.required' => '请上传封面图',
            'headerImg.required' => '头像不能为空',
            'nickname.required' => '昵称不能为空'
        ];
    }

    public function attributes()
    {
        return [
            'imageUrl' => $this->imageUrl,
            'headerImg' => $this->headerImg,
            'nickname' => $this->nickname,
            'mobile' => empty($this->mobile) ? '': $this->mobile,
            'qrCodeUrl' => $this->qrCodeUrl,
            'companyName' => empty($this->companyName) ? '获客宝典' : $this->companyName
        ];
    }

}
