<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\ApiFormRequestBase;
use Illuminate\Foundation\Http\FormRequest;

class ComplaintCreateRequest extends ApiFormRequestBase
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'      => 'required|string'
        ];
    }

    public function messages()
    {
        return  [
            'type.required'    => '必须填写“类型”',
        ];
    }

    public function attributes()
    {
        return [
            'type' => $this->type,
            'articleVideoId' => empty($this->articleVideoId) ? '' : $this->articleVideoId,
            'body' => empty($this->body) ? '' : $this->body,
            'emailMobile' => empty($this->emailMobile) ? '' : $this->emailMobile,
            'imageUrlArr' => empty($this->imageUrlArr) ? [] : $this->imageUrlArr
        ];
    }
}
