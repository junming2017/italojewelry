<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\ImageUpdateRequest;
use App\Repositories\ImageRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

/**
 * ArticleVideoCartoonController.
 *
 * @author 肖俊明<2284876299@qq.com>
 * @since  2020年09月01日
 */
class ImageController extends BaseController
{

    protected $imageRepository;

    /**
     * ImageController constructor.
     * @param ImageRepository $imageRepository
     */
    public function __construct(ImageRepository $imageRepository)
    {
        $this->imageRepository = $imageRepository;
    }




    /**
     * 审核中的文章.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author 肖俊明<2284876299@qq.com>
     * @since 2020年03月25日
     */
    public function index()
    {
        return view('backend.image.index');
    }


    /**
     * 图片初始化.
     *
     * @return \Illuminate\Http\JsonResponse
     * @author 肖俊明<2284876299@qq.com>
     * @since 2021年07月01日
     */
    public function init()
    {
        $category = [
            [
                'value'=> 'ENGAGEMENT',
                'label' => 'ENGAGEMENT',
                'children' => [
                    [
                        'value'=> 'CLASSIC',
                        'label' => 'CLASSIC',
                    ],
                    [
                        'value'=> 'UNIQUE',
                        'label' => 'UNIQUE',
                    ],
                    [
                        'value'=> 'HALO',
                        'label' => 'HALO',
                    ],
                    [
                        'value'=> 'THREE STONE',
                        'label' => 'THREE STONE',
                    ],
                    [
                        'value'=> 'OVAL CUT',
                        'label' => 'OVAL CUT',
                    ],
                    [
                        'value'=> 'EMERALD CUT',
                        'label' => 'EMERALD CUT',
                    ],
                    [
                        'value'=> 'ROSE GOLD',
                        'label' => 'ROSE GOLD',
                    ],
                    [
                        'value'=> 'VINTAGE',
                        'label' => 'VINTAGE',
                    ],
                ]
            ],
            [
                'value'=> 'WEDDING BANDS',
                'label' => 'WEDDING BANDS',
                'children' => [
                    [
                        'value'=> 'ETERNITY BAND',
                        'label' => 'ETERNITY BAND',
                    ],
                    [
                        'value'=> 'ANNIVERSARY BAND',
                        'label' => 'ANNIVERSARY BAND',
                    ],
                    [
                        'value'=> 'MULTI ROW BAND',
                        'label' => 'MULTI ROW BAND',
                    ],
                    [
                        'value'=> 'CLASSIC BAND',
                        'label' => 'CLASSIC BAND',
                    ],
                ]
            ],
            [ //3
                'value'=> 'WEDDING SETS',
                'label' => 'WEDDING SETS',
                'children' => [
                    [
                        'value'=> 'BRIDAL SETS',
                        'label' => 'BRIDAL SETS',
                    ],
                    [
                        'value'=> '3PC WEDDING SETS',
                        'label' => '3PC WEDDING SETS',
                    ],
                    [
                        'value'=> 'STACKABLE RING SETS',
                        'label' => 'STACKABLE RING SETS',
                    ],
                    [
                        'value'=> 'TRIO RING SETS',
                        'label' => 'TRIO RING SETS',
                    ],
                ]
            ],
            [ //4
                'value'=> 'JEWELRY',
                'label' => 'JEWELRY',
                'children' => [
                    [
                        'value'=> 'NECKLACES',
                        'label' => 'NECKLACES',
                    ],
                    [
                        'value'=> 'EARRINGS',
                        'label' => 'EARRINGS',
                    ],
                    [
                        'value'=> 'BRACELETS',
                        'label' => 'BRACELETS',
                    ],
                    [
                        'value'=> 'COUPLE RINGS',
                        'label' => 'COUPLE RINGS',
                    ],
                ]
            ],
            [ //5
                'value'=> 'MEN',
                'label' => 'MEN',
                'children' => [
                    [
                        'value'=> 'RINGS',
                        'label' => 'RINGS',
                    ],
                    [
                        'value'=> 'CUFFLINKS',
                        'label' => 'CUFFLINKS',
                    ],
                ]
            ],
            [ //6
                'value'=> 'PERSONALIZED',
                'label' => 'PERSONALIZED',
                'children' => [
                    [
                        'value'=> 'PERSONALIZED NECKLACES',
                        'label' => 'PERSONALIZED NECKLACES',
                    ],
                    [
                        'value'=> 'PERSONALIZED BRACELETS',
                        'label' => 'PERSONALIZED BRACELETS',
                    ],
                    [
                        'value'=> 'PERSONALIZED EARRINGS',
                        'label' => 'PERSONALIZED EARRINGS',
                    ],
                ]
            ],
            [ //7
                'value'=> 'COLLECTION',
                'label' => 'COLLECTION',
                'children' => [
                    [
                        'value'=> 'DANCING BUTTERFLY',
                        'label' => 'DANCING BUTTERFLY',
                    ],
                    [
                        'value'=> 'BLOOM',
                        'label' => 'BLOOM',
                    ],
                    [
                        'value'=> 'PERSONALIZED EARRINGS',
                        'label' => 'PERSONALIZED EARRINGS',
                    ],
                ]
            ],
        ];
        return $this->responseSuccess($category);
    }

    /**
     * postList.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @author 肖俊明<2284876299@qq.com>
     * @since 2020年03月09日
     */
    public function postList(Request $request)
    {

        $limit = (int)$request->get('limit', 10);
        $orderField = $request->get('orderField', 'id');
        $order = $request->get('order', 'desc');
        $keyword = $request->get('keyword', '');
        $bigName = $request->get('bigName', '');
        $midName = $request->get('midName', '');

        $where = [];

        !empty($keyword) && $where['keyword'] = $keyword;

        !empty($bigName) && $where['big_name'] = $bigName;
        !empty($midName) && $where['mid_name'] = $midName;

        $images = $this->imageRepository->getList($where, $limit, $orderField, $order);

        $skuIds = [231111,231113,241208,241202,141007,241205,221111,211229,211340,211278,211282,211237,211384,211172,241191,241276,241152,241145,231096,211402,241304,241233,211395,211344,241296,241291,211205,241165,211122,211395,241237,241146,141006,211228,241110,211277,211255,241107,241117,211381,211173,211077,211162,211079,211161,211231,211256,211230,211261,211206,211217,211239,141004,211159,211158,141005,211163,241212,241268,231056,221052,221121,211222,211352,211349,241109,241118,241168,241132];

        $images->map(function ($item) use ($skuIds){

            $item->color = in_array($item->sku_id,$skuIds) ? 1 : false;
            $images = json_decode($item['images'],true);
            $item->ringInformations = json_decode($item['ring_information'],true);
            $item->gemInformations = json_decode($item['gem_information'],true);
            $item->imageUrl = count($images)==0 ? '': str_replace("/home/gopath/italojewelry","",(array_values($images)[0]));

            $item->price = empty($item->price) ? '--': $item->price;
            $item->retail_price = empty($item->retail_price) ? '--': $item->retail_price;
            $item->save_price = empty($item->save_price) ? '--': $item->save_price;
        });

        return $this->responseSuccess($images);
    }

    /**
     * 新增备注.
     *
     * @param string $id
     * @param ImageUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @author 肖俊明<2284876299@qq.com>
     * @since 2021年07月02日
     */
    public function update(string $id, ImageUpdateRequest $request)
    {
        $image = $this->imageRepository->getInfoById($id);
        if (empty($image)) {
            return $this->responseNotFound('找不到该数据');
        }
        $attributes = $request->attributes();
        foreach ($attributes as $key => $value) {
            $image->$key = $value;
        }
        $result = $image->save() ? true:false;
        return $this->responseSuccess($result);
    }

    public function show($id)
    {
        return view('backend.image.detail',compact('id'));
    }

    public function detail($id)
    {
        $image = $this->imageRepository->getInfoById($id);
        if (empty($image)) {
            return $this->responseNotFound('找不到该数据');
        }
        $images = json_decode($image->images,true);

        foreach ($images as $key=>$item){
            $images[$key] = str_replace("/home/gopath/italojewelry","",$item);
        }
        $image->images = $images;


        return $this->responseSuccess($image);
    }



}
