<?php

namespace App\Http\Middleware;

use Closure;
//use \Tymon\JWTAuth\Http\Middleware\Authenticate as JWTAuthAuthenticate;

class JWTAuthenticate extends \Tymon\JWTAuth\Http\Middleware\Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        try {
            $this->authenticate($request);
        } catch (\Exception $exception) {

            if (!empty($request->header('Authorization')) && $request->header('Authorization') != 'Bearer token') {
                $code = 402;
            } else {
                $code = 401;
                if ($exception->getMessage() == 'Token not provided') {
                    $code = 402;
                }
            }
            $error = [
                'status' => 'failed',
                'error' => [
                    'code' => $code,
                    'msg' => $exception->getMessage()
                ]
            ];
            return response()->json($error, $code, []);
        }
        return $next($request);
    }
}
