<?php

namespace App\Providers;

use App\Events\ArticleImageQrCodeEvent;
use App\Events\ArticleVideoVisitEvent;
use App\Events\GoodMNightLogEvent;
use App\Events\IncomeRetailEvent;
use App\Events\InvitationRelationEvent;
use App\Events\OpenVipEvent;
use App\Events\WechatUserLoginEvent;
use App\Listeners\ArticleImageQrCodeListener;
use App\Listeners\ArticleVideoVisitListener;
use App\Listeners\GoodMNightListener;
use App\Listeners\IncomeRetailListener;
use App\Listeners\InvitationRelationListener;
use App\Listeners\OpenVipListener;
use App\Listeners\QueryListener;
use App\Listeners\WechatUserLoginListener;
use App\Models\Income;
use App\Models\Invitation;
use App\Models\Order;
use App\Models\WechatUser;
use App\Observers\IncomeObserver;
use App\Observers\InvitationObserver;
use App\Observers\OrderObserver;
use App\Observers\WechatUserObserver;
use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        QueryExecuted::class => [
            QueryListener::class
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //Song::observe(new SongObserver());
    }
}
