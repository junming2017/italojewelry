<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * 文章视频动画数据.
 *
 * @author 肖俊明<2284876299@qq.com>
 * @since  2020年08月31日
 */
class Image extends Model
{

    /**
     * 表名.
     *
     * @var string
     */
    public $table = "italojewelry";



    /**
     * The attributes that are mass assignable.
     * originalUrl 原文地址
     * fromAccount 来源账号
     * source 来源
     * type 类型，1文章，2视频，3动画，4课程,5图片,6早晚安,7课件
     * title 标题
     * describe 摘要
     * imageUrl 图片
     * videoUrl mp4
     * body 内容数据
     * companyTags 品牌id
     * categoryTags 分类ID
     * userId 微信用户ID 添加文章的用户
     * wxView 微信访问人数
     * collectedNum 收藏人数
     * editedNum 编辑人数
     * shareNum 分享人数
     * articleVideoId 原文章地址
     * isDel 是否删除0不是，1处于删除状态
     * status 1显示，0隐藏
     * verifyStatus 1审核通过，0审核中，-1 审核不通过
     * pushStatus 0表示未推送，1已经推送
     * catId 当早晚安时候才有的，0表示早安，1晚安
     *
     * @var array
     */
    protected $fillable = [
        'default_name', 'big_name', 'mid_name', 'detail_url', 'title', 'sku_id', 'price', 'retail_price',
        'save_price', 'ring_information', 'gem_information','images','videos'
    ];
}
