<?php


Route::group(['namespace' => 'Backend', 'as' => 'admin.'], function ($route) {


    $route->group(['middleware' => 'auth.backend'], function ($route) {
        $route->get('/',['as' => 'dashboard', 'uses' => 'DashboardController@index']);
    });

    //图片部分imageIndex
    $route->get('image/index',['as'=>'image.index', 'uses'=> 'ImageController@index']);
    $route->get('image/init',['as'=>'image.init', 'uses'=> 'ImageController@init']);
    $route->post('image/post-list',['as'=>'image.postList', 'uses'=> 'ImageController@postList']);
    $route->put('image/{id}',['as'=>'image.update', 'uses'=> 'ImageController@update']);
    $route->get('image/{id}',['as'=>'image.show', 'uses'=> 'ImageController@show']);
    $route->get('image/detail/{id}',['as'=>'image.detail', 'uses'=> 'ImageController@detail']);

    $route->get('/login',['as' => 'user.login', 'uses' => 'Auth\LoginController@showLoginForm']);
    $route->post('/login', ['as' => 'user.login', 'uses' => 'Auth\LoginController@login']);//->middleware('throttle:5,1');
    $route->get('/logout',['as' => 'user.logout', 'uses' => 'Auth\LoginController@logout']); //退出
    $route->get('/login-captcha', ['as' => 'user.captcha','uses' => 'Auth\LoginController@captcha']);
});
